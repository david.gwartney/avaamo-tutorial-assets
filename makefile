SOURCES=my-assistant.html ask-ivy-avatar.png visa-mastercard-classic-gold-platinum-en.pdf tutorial.pdf create_incident.js returning_user.js
ZIP_FILE=avaamo-training.zip

zip:
	zip $(ZIP_FILE) $(SOURCES)

clean:
	$(RM) $(ZIP_FILE)
